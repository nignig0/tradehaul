from django.shortcuts import render,redirect, HttpResponse
from django.contrib.auth import login, authenticate
from TH.forms import*
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import JsonResponse
from django.core.mail import send_mail
from django.conf import settings
# Create your views here.
def home(request):
	
	return render(request, 'TH/home.html')

def register(request):
	if request.method =='POST':
		form = CustomUserCreationForm(request.POST)
		if form.is_valid():
			info = form.save(commit=False)#information about the new user
			u = User.objects.create(username = info.username, password = info.password,email=info.email, first_name=info.first_name, last_name=info.last_name, phone_no = info.phone_no)

			#authenticating the new user
			username = form.cleaned_data['username']
			raw_password = form.cleaned_data['password1']
			user = authenticate(username=username, password = raw_password)
			c = Customer.objects.create(user = user)
			login(request, user)
			return redirect('/')
		else:
			return render(request, 'TH/register.html', {'form':form})
	else:
		form = CustomUserCreationForm()
		args = {"form":form}
		return render(request, 'TH/register.html', args)

def search(request):
	query = request.GET.get("q")
	if query:
		results = Product.objects.filter(Q(name__icontains=query) | Q(description__icontains=query))
		return render(request, 'TH/results.html', {'results':results})
	else:
		return redirect('/')

def product_details(request, pk):
	product = Product.objects.get(pk = pk)
	customer = Customer.objects.get(user = request.user)
	cart = customer.cart.all()
	return render(request, 'TH/product_details.html', {'product':product, 'cart':cart})

@login_required(login_url = '/login/')
def shop(request, category):
	customer = Customer.objects.get(user = request.user)
	if not Order.objects.filter(customer=customer).exists():
		order = Order.objects.create(customer = customer, total_price=0)#create the order object
	cart = customer.cart.all() #The list of items in the cart
	cat = Category.objects.get(name = category)
	product_set = cat.product_set.all().order_by('price') #The set of products under the category
	
	paginator = Paginator(product_set, 12)
	page = request.GET.get('page')
	

	if page ==None:
		page = 1

	products = paginator.get_page(page)

	categories = Category.objects.all()
	favorites = customer.favorites.all()
	args = {'category_m':cat, 'categories': categories,'products':products ,'cart':cart, 'favorites':favorites} 
	return render(request, 'TH/shop.html', args)

@login_required(login_url='/login/')
def checkout(request):
	customer = Customer.objects.get(user = request.user)
	
	if not Order.objects.filter(customer=customer).exists() or len(customer.cart.all()) == 0:
		return render(request,'TH/error.html')
	else:
		price=0 
		for products in customer.cart.all(): #calculating the price of everything in the
			price+=products.price			#cart with the quantity ordered being 1				
		
		order = Order.objects.get(customer=customer)
		order.total_price = price #put the total price for the base as order price to ensure consistency
		order.description=""
		for product in customer.cart.all(): #to create the description message
			order.description +="amount ordered:1 product: "+product.name+" product_price: "+str(product.price)+"\n"
		order.save()

		cart = customer.cart.all()
		args = {'cart': cart, 'price':order.total_price,'pk':order.pk}
		return render(request, 'TH/checkout.html', args) 

def process_checkout(request, key):
	customer = Customer.objects.get(user = request.user)
	order = Order.objects.get(customer = customer)
	transaction = Transaction.objects.create(customer = order.customer, total_price=order.total_price, description=order.description)
	transaction.description +="Total Price: "+str(order.total_price)+'\n'
	order.delete()# delete the order
	customer.cart.clear()
	#send the mail
	subject = 'Order from '+request.user.username
	message = transaction.description
	from_email = settings.EMAIL_HOST_USER
	recipients_list = ['tanitoluwaadebayo@gmail.com', request.user.email]
	send_mail(subject,message, from_email, recipients_list)
	if key=='cashless':
		return redirect('somewhere/')
	else:
		return redirect('/')

def add_cart(request):
	pk = request.POST.get('pk') #get the primary key from the ajax post request
	product = Product.objects.get(pk=pk) #get the product from the database
	customer = Customer.objects.get(user = request.user) #to make changes to the customer
	customer.cart.add(product) #add the product to the customers cart
	customer.save() #save the customer object
	return HttpResponse('success')

def remove_cart(request):
	pk = request.POST.get('pk') #get the primary key from ajax post request
	product = Product.objects.get(pk=pk) #get the product from the database
	customer = Customer.objects.get(user=request.user) #to make changes to the customer
	customer.cart.remove(product) #remove the product from the customers cart
	customer.save() #save the customer object
	return HttpResponse('success')

def get_price(request, key):
	order_pk = request.POST.get('opk')
	product_pk = request.POST.get('pk')
	qty = request.POST.get('qty')
	order = Order.objects.get(pk=order_pk)
	price = order.total_price
	customer = Customer.objects.get(user=request.user)
	product = Product.objects.get(pk = product_pk)
	b_price = 0
	for products in customer.cart.all():
		b_price+= products.price
	if key=='M':
		increments = price - b_price
		increments = increments-product.price
		price = increments+b_price
		order.total_price = price
		order.description +="Change... \n amount ordered: "+str(qty)+ "product: "+product.name+" product price: "+str(product.price)+"\n"
		order.save()
		return JsonResponse({'price':price})
	elif key=='A':
		increments = price - b_price
		increments = increments+product.price
		price = increments+b_price
		order.total_price = price
		order.description +="Change... \n amount ordered: "+str(qty)+ "product: "+product.name+" product price: "+str(product.price)+"\n"
		order.save()
		return JsonResponse({'price':price})


def sort(request,category,key): #the key here is the sort_by option
	cat = Category.objects.get(name = category) #get the category that this request was made from
	categories = Category.objects.all()
	customer = Customer.objects.get(user = request.user)
	cart = customer.cart.all()
	if key == 'cheapest':
		products_set = cat.product_set.all().order_by('price')
		
		paginator = Paginator(products_set, 12)
		page = request.GET.get('page')
		if page ==None:
			page = 1

		products = paginator.get_page(page)

		args = {'category_m':cat, 'categories': categories,'products':products ,'cart':cart} 
		return render(request, 'TH/shop.html',args)
	elif key == 'newest':
		products_set = cat.product_set.all().order_by('created_at','updated_at')

		paginator = Paginator(products_set, 12)
		page = request.GET.get('page')
		if page ==None:
			page = 1

		products = paginator.get_page(page)

		args = {'category_m':cat, 'categories': categories,'products':products ,'cart':cart} 
		return render(request, 'TH/shop.html',args)
	elif key=='most_popular':
		pass

def add_favorite(request):
	pk = request.POST.get('pk') #get the primary key from the ajax post request
	product = Product.objects.get(pk=pk) #get the product from the database
	product.num_favorites = product.num_favorites+1
	product.save()
	customer = Customer.objects.get(user = request.user) #to make changes to the customer
	customer.favorites.add(product) #add the product to the customers cart
	customer.save() #save the customer object
	return HttpResponse('success')

def remove_favorite(request):
	pk = request.POST.get('pk') #get the primary key from ajax post request
	product = Product.objects.get(pk=pk) #get the product from the database
	product.num_favorites = product.num_favorites-1
	product.save()
	customer = Customer.objects.get(user=request.user) #to make changes to the customer
	customer.favorites.remove(product) #remove the product from the customers cart
	customer.save() #save the customer object
	return HttpResponse('success')

def favorites(request):
	customer = Customer.objects.get(user = request.user)
	favorites = customer.favorites.all()
	args = {'favorites':favorites}
	return render(request,'TH/favorites.html', args)