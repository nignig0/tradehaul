from django.db import models
from django.contrib.auth.models import AbstractUser


class Category(models.Model):
	name = models.CharField(max_length=50)


	def __str__(self):
		return self.name

class Product(models.Model):
	name = models.CharField(max_length=50)
	description   = models.TextField()
	price = models.IntegerField()
	picture = models.ImageField(upload_to='media/', default='C:/Users/USER/Desktop/shawarma/')
	category = models.ForeignKey(Category, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	num_favorites  = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.category.name+'_'+self.name +' '+str(self.price)
	def get_absolute_url(self):
		return '/products/'+str(self.pk)+'/'

class User(AbstractUser):
	phone_no = models.CharField(max_length = 14)

class Customer(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	cart = models.ManyToManyField(Product) #cart holds many products andmany products can be in carts
	favorites = models.ManyToManyField(Product, related_name='favorites')

	def __str__(self):
		return self.user.username

class Order(models.Model): #for storing the transaction that a customer makes
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
	total_price = models.IntegerField()
	description = models.TextField()

class Transaction(models.Model): #for storing the transaction that a customer makes
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
	total_price = models.IntegerField()
	description = models.TextField()

