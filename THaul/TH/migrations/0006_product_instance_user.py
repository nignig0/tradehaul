# Generated by Django 2.0.6 on 2019-07-09 18:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TH', '0005_auto_20190709_1914'),
    ]

    operations = [
        migrations.AddField(
            model_name='product_instance',
            name='user',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
