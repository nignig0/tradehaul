# Generated by Django 2.0.6 on 2019-07-04 10:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TH', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='picture',
            field=models.ImageField(default='C:/Users/USER/Desktop/shawarma/', upload_to='media/'),
        ),
    ]
