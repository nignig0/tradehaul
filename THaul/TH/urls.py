from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth.views import login, logout
from django.conf import settings 
from django.conf.urls.static import static 

urlpatterns = [
   path('', views.home),
   path('register/', views.register),
   path('login/', login, {'template_name':'TH/login.html'}),
   path('logout/',logout,{'template_name': 'TH/home.html'}),
   path('results/', views.search),
   path('products/<str:category>/',views.shop),
   path('products/details/<int:pk>/', views.product_details),
   path('add_cart/', views.add_cart),
   path('remove_cart/', views.remove_cart),
   path('checkout/', views.checkout),
   path('get_price/<str:key>/', views.get_price),
   path('process_checkout/<str:key>/', views.process_checkout),
   path('products/<str:category>/<str:key>/', views.sort),
   path('add_favorite/', views.add_favorite),
   path('remove_favorite/', views.remove_favorite),
   path('favorites/', views.favorites)
]
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)