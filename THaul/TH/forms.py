from django import forms
from django.forms import ModelForm
from TH.models import *
from django.contrib.auth.forms import UserCreationForm

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model=User
		fields = ['username','password1','password2','first_name', 'last_name','email','phone_no']