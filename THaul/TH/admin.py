from django.contrib import admin
from TH.models import *

admin.site.register(User)
admin.site.register(Category)
admin.site.register(Customer)
admin.site.register(Product)
admin.site.register(Order)
